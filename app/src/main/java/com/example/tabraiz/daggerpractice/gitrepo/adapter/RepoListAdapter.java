package com.example.tabraiz.daggerpractice.gitrepo.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tabraiz.daggerpractice.R;
import com.example.tabraiz.daggerpractice.gitrepo.models.Repository;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by tabraiz on 25/08/18.
 */

public class RepoListAdapter extends RecyclerView.Adapter<RepoListAdapter.RepoViewHolder> {


    List<Repository> mRepoList = new ArrayList<>();
    Context mContext;
    Picasso picasso;

    public RepoListAdapter(List<Repository> mRepoList, Context context, Picasso picasso) {
        this.mRepoList = mRepoList;
        this.mContext = context;
        this.picasso = picasso;
    }


    @NonNull
    @Override
    public RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_item,parent,false);
        return new RepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoViewHolder holder, int position) {
        Repository repository = mRepoList.get(position);
        holder.repoName.setText(repository.getName());
        picasso.load(repository.getOwner().getAvatarUrl()).placeholder(R.drawable.placeholder_user_image)
                .into(holder.userAvatar);
        holder.repoDescription.setText(repository.getDescription());
        holder.repoForks.setText("10");
        holder.repoUpdatedDate.setText( new Date().toString());
        holder.repoStars.setText("1");
        holder.repoIssues.setText("0");
    }

    @Override
    public int getItemCount() {
        return mRepoList.size();
    }

    class RepoViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.repo_name)
        TextView repoName;

        @BindView(R.id.user_avatar)
        ImageView userAvatar;

        @BindView(R.id.repo_description)
        TextView repoDescription;

        @BindView(R.id.repo_updated_date)
        TextView repoUpdatedDate;

        @BindView(R.id.repo_stars)
        TextView repoStars;

        @BindView(R.id.repo_forks)
        TextView repoForks;

        @BindView(R.id.repo_issues)
        TextView repoIssues;

        RepoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
