package com.example.tabraiz.daggerpractice.gitrepo.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.tabraiz.daggerpractice.R;
import com.example.tabraiz.daggerpractice.application.DaggerApplication;
import com.example.tabraiz.daggerpractice.base.views.BaseFragment;
import com.example.tabraiz.daggerpractice.gitrepo.adapter.RepoListAdapter;
import com.example.tabraiz.daggerpractice.gitrepo.models.Repository;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tabraiz on 25/08/18.
 */

public class RepoListFragment extends BaseFragment{

    @BindView(R.id.repo_list)
    RecyclerView repoList;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    List<Repository> mRepoList = new ArrayList<>();

    RepoListAdapter repoListAdapter;
    private Call<List<Repository>> repoListCall;

    public RepoListFragment(){
        /*default public constructor*/
    }

    public static RepoListFragment newInstance() {
        return  new RepoListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_repo_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar.setVisibility(View.VISIBLE);
        repoListCall = DaggerApplication.get(getActivity()).getGitHubService().getRepoList("0");
        final Picasso picasso =  DaggerApplication.get(getActivity()).getPicasso();
        repoListCall.enqueue(new Callback<List<Repository>>() {
            @Override
            public void onResponse(Call<List<Repository>> call, Response<List<Repository>> response) {
                    mRepoList = response.body();
                    progressBar.setVisibility(View.INVISIBLE);
                    repoListAdapter = new RepoListAdapter(mRepoList, getContext(), picasso);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplication());
                    RepoListFragment.this.repoList.setLayoutManager(layoutManager);
                    RepoListFragment.this.repoList.setAdapter(repoListAdapter);

            }

            @Override
            public void onFailure(Call<List<Repository>> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(repoListCall != null)
            repoListCall.cancel();
    }
}
