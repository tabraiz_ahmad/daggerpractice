package com.example.tabraiz.daggerpractice;

import android.os.Bundle;

import com.example.tabraiz.daggerpractice.base.views.BaseActivity;
import com.example.tabraiz.daggerpractice.gitrepo.views.RepoListFragment;

public class HomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        if(savedInstanceState == null)  {
            replaceFragment(R.id.fragment_container, RepoListFragment.newInstance(),
                    false);
        }
    }
}
