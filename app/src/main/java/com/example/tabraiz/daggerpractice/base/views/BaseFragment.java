package com.example.tabraiz.daggerpractice.base.views;

import android.support.v4.app.Fragment;

import butterknife.Unbinder;

/**
 * Created by tabraiz on 25/08/18.
 */

public class BaseFragment extends Fragment {
    protected Unbinder unbinder = null;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}
