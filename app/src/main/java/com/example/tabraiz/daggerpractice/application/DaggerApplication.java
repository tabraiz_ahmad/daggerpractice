package com.example.tabraiz.daggerpractice.application;

import android.app.Activity;
import android.app.Application;
import com.example.tabraiz.daggerpractice.network.WebService;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by tabraiz on 25/08/18.
 */

public class DaggerApplication extends Application {

    private Retrofit gitHubRestClient;
    private static String BASE_URL = "https://api.github.com/";
    private Picasso picasso;
    private WebService gitHubService;



    public static DaggerApplication get(Activity activity){
        return (DaggerApplication) activity.getApplication();
    }


    //Juice,Dagger 2, Spring

    //Dependency Tree


            //Activity

        //GitHubService

    // GsonConverterFactory    //Picasso

    //Retrofit

                //OKHttp

    //Logger             //Cache

    //Timber                    //File


    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Timber.e(message);
            }
        });
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                                        .addInterceptor(httpLoggingInterceptor)
                                        .build();

        picasso = new Picasso.Builder(this)
                .downloader(new OkHttp3Downloader(okHttpClient))
                .build();

        gitHubRestClient = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        gitHubService = gitHubRestClient.create(WebService.class);

    }



    public WebService getGitHubService() {
        return gitHubService;
    }

    public Picasso getPicasso() {
        return picasso;
    }
}
