package com.example.tabraiz.daggerpractice.network;

import com.example.tabraiz.daggerpractice.gitrepo.models.Repository;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by tabraiz on 25/08/18.
 */

public interface WebService {
    @GET("repositories")
    Call< List<Repository> > getRepoList(@Query("since") String since);

}
